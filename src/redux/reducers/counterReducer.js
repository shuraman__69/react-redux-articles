const INCREMENT = 'INCREMENT'
const DECREMENT = 'DECREMENT'

const initialState = {
    counter: 0
}

export const counterReducer = (state = initialState, action) => {
    switch (action.type) {
        case INCREMENT: {
            return {
                ...state,
                counter: state.counter + action.count
            }
        }
        case DECREMENT: {
            return {
                ...state,
                counter: state.counter - action.count
            }
        }
        default:
            return state;
    }
}
export const increment = (count) => ({type: INCREMENT, count})
export const decrement = (count) => ({type: DECREMENT, count})
