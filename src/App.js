import './App.css';
import {Route, Switch} from 'react-router-dom'
import {routes} from "./routes";

function App() {
    return (
        <Switch>
            <div className='container text-center pt-5'>
                {routes.map(route => <Route path={route.href} render={() => route.component}/>)}
            </div>
        </Switch>
    );
}


export default App;
