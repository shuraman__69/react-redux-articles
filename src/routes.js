import {ReduxIntro} from "./articles/01_redux_intro";

export const routes = [
    {
        href: '/redux-intro',
        component: <ReduxIntro/>
    }
]
