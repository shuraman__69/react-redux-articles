import {useDispatch, useSelector} from "react-redux";
import {decrement, increment} from "../../redux/reducers/counterReducer";

export const ReduxIntro = () => {
    const dispatch = useDispatch()
    const counter = useSelector(state => state.counter.counter)
    return (
        <>
            <h2>Counter</h2>
            <div>
                <input type="text" className='text-center' value={counter}/>
                <div className='d-flex justify-content-center'>
                    <div className='m-2 text' onClick={() => dispatch(decrement(5))}>-</div>
                    <div className='m-2 text' onClick={() => dispatch(increment(3))}>+</div>
                </div>
            </div>
        </>
    )
}
